package deutschebank.dbutils;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import junit.framework.TestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class PropertyLoaderTest extends TestCase {
	 private PropertyLoader pLoader;
	
	 @Override
	    protected void setUp() throws Exception {
		 pLoader = PropertyLoader.getLoader();
		 assertNotNull(pLoader);
	    }

	public void testSmokePropertyLoader() throws IOException
	{
		 Properties pp = pLoader.getPropValues( "dbConnector.properties" );
		 assertNotNull(pp);
		 assertNotNull( pp.getProperty("dbDriver"));
	}
	
	public void testSmokePropertyLoader1() throws IOException
	{
		 Properties pp = pLoader.getPropValues( "not_exist" );
		 assertNotNull(pp);
	}
}