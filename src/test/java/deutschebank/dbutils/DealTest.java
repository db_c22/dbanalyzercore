package deutschebank.dbutils;

import java.util.ArrayList;

import junit.framework.TestCase;

public class DealTest extends TestCase {

    private static final int DEAL_ID = 0;
    private static final String DEAL_TIME = "2018-07-30T05:58:20.915";
    private static final int DEAL_COUNTERPARTY_ID = 0;
    private static final int DEAL_INSTRUMENT_ID = 0;
    private static final String DEAL_TYPE = "B";
    private static final int DEAL_AMOUNT = 0;
    private static final int DEAL_QUANTITY = 0;

    private Deal deal;

    @Override
    public void setUp()
    {
        deal = new Deal(DEAL_ID, DEAL_TIME, DEAL_COUNTERPARTY_ID,
                DEAL_INSTRUMENT_ID, DEAL_TYPE, DEAL_AMOUNT, DEAL_QUANTITY);
        assertNotNull(deal);
    }

    public void testGetDealID()
    {
        assertEquals(DEAL_ID, deal.getDealID());
    }

    public void testGetDealTime()
    {
        assertEquals(DEAL_TIME, deal.getDealTime());
    }

    public void testGetCounterpartyID()
    {
        assertEquals(DEAL_COUNTERPARTY_ID, deal.getDealCounterpartyID());
    }

    public void testGetInstrumentID()
    {
        assertEquals(DEAL_INSTRUMENT_ID, deal.getDealInstrumentID());
    }

    public void testGetDealType()
    {
        assertEquals(DEAL_TYPE, deal.getDealType());
    }

    public void testGetDealAmount()
    {
        assertEquals(DEAL_AMOUNT, deal.getDealAmount());
    }

    public void testGetDealQuantity()
    {
        assertEquals(DEAL_QUANTITY, deal.getDealQuantity());
    }
    

    public void testSetDealID()
    {
    	int newDealID = 1;
    	deal.setDealID(newDealID);
        assertEquals(newDealID, deal.getDealID());
    }

    public void testSetDealTime()
    {
    	String newDealTime = "2018-07-30T05:58:21.947";
    	deal.setDealTime(newDealTime);
        assertEquals(newDealTime, deal.getDealTime());
    }

    public void testSetCounterpartyID()
    {
    	int newCounterpartyID = 1;
    	deal.setDealCounterpartyID(newCounterpartyID);
        assertEquals(newCounterpartyID, deal.getDealCounterpartyID());
    }

    public void testSetInstrumentID()
    {
    	int newInstrumentID = 1;
    	deal.setDealInstrumentID(newInstrumentID);
        assertEquals(newInstrumentID, deal.getDealInstrumentID());
    }

    public void testSetDealType()
    {
    	String newDealType = "S";
    	deal.setDealType(newDealType);
        assertEquals(newDealType, deal.getDealType());
    }

    public void testSetDealAmount()
    {
    	int newDealAmount = 1;
    	deal.setDealAmount(newDealAmount);
        assertEquals(newDealAmount, deal.getDealAmount());
    }

    public void testSetDealQuantity()    
    {
    	int newDealQuantity = 1;
    	deal.setDealQuantity(newDealQuantity);
        assertEquals(newDealQuantity, deal.getDealQuantity());
    }
    
    public void testDealToJson() {
        String result = DealHandler.getLoader().toJSON(deal);
        assertNotNull(result);
    }


    public void testArrayListDealsToJson() {
        ArrayList<Deal> testDeals = new ArrayList<Deal>();
        testDeals.add(deal);
        testDeals.add(new Deal(1, DEAL_TIME, 1,
                1, "S", 1, 1));
        String result = DealHandler.getLoader().toJSON(testDeals);
        assertNotNull(result);
    }
}
