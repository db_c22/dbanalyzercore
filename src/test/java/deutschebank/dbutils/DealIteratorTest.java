package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jmock.Mockery;
import org.jmock.Expectations;
import static org.junit.Assert.assertNotEquals;

import junit.framework.TestCase;

public class DealIteratorTest extends TestCase {
	
	   	private static final int DEAL_ID = 0;
	    private static final String DEAL_TIME = "2018-07-30T05:58:20.915";
	    private static final int DEAL_COUNTERPARTY_ID = 1;
	    private static final int DEAL_INSTRUMENT_ID = 2;
	    private static final String DEAL_TYPE = "B";
	    private static final int DEAL_AMOUNT = 3;
	    private static final int DEAL_QUANTITY = 4;
	    private static final boolean TEST_BOOLEAN_VALUE = true;
	
	Mockery context = new Mockery();
	final ResultSet rowIterator = context.mock(ResultSet.class);
	DealIterator testDealIterator = new DealIterator(rowIterator);
	
	public void testDealIteratorGetUserID() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getInt("deal_id"); will(returnValue(DEAL_ID));
		}});
        
        context.assertIsSatisfied();
        assertEquals(DEAL_ID, testDealIterator.getDealID());
	}
	
	
	public void testDealIteratorGetDealTime() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("deal_time"); will(returnValue(DEAL_TIME));
		}});
        
        context.assertIsSatisfied();
        assertEquals(DEAL_TIME, testDealIterator.getDealTime());
	}
	
	public void testDealIteratorGetDealType() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("deal_type"); will(returnValue(DEAL_TYPE));
		}});
        
        context.assertIsSatisfied();
        assertEquals(DEAL_TYPE, testDealIterator.getDealType());
	}
	
	public void testDealIteratorGetInstrumentID() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getInt("deal_instrument_id"); will(returnValue(DEAL_INSTRUMENT_ID));
		}});
        
        context.assertIsSatisfied();
        assertEquals(DEAL_INSTRUMENT_ID, testDealIterator.getDealInstrumentID());
	}
	
	public void testDealIteratorGetDealCounterpartyID() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getInt("deal_counterparty_id"); will(returnValue(DEAL_COUNTERPARTY_ID));
		}});
        
        context.assertIsSatisfied();
        assertEquals(DEAL_COUNTERPARTY_ID, testDealIterator.getDealCounterpartyID());
	}
	
	public void testDealIteratorGetAmount() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getInt("deal_amount"); will(returnValue(DEAL_AMOUNT));
		}});
        
        context.assertIsSatisfied();
        assertEquals(DEAL_AMOUNT, testDealIterator.getDealAmount());
	}
	
	public void testDealIteratorGetQuantity() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getInt("deal_quantity"); will(returnValue(DEAL_QUANTITY));
		}});
        
        context.assertIsSatisfied();
        assertEquals(DEAL_QUANTITY, testDealIterator.getDealQuantity());
	}
	
	public void testDealIteratorFirst() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).first(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        DealIterator testDealIterator = new DealIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testDealIterator.first());
	} 
	
	public void testDealIteratorLast() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).last(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        DealIterator testDealIterator = new DealIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testDealIterator.last());
	} 
	
	public void testDealIteratorNext() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).next(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        DealIterator testDealIterator = new DealIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testDealIterator.next());
	} 
	
	public void testDealIteratorPrior() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).previous(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        DealIterator testDealIterator = new DealIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testDealIterator.prior());
	}

}
