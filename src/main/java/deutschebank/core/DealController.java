/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.core;

import deutschebank.dbutils.Deal;
import deutschebank.dbutils.DealHandler;
import java.util.ArrayList;

/**
 *
 * @author Graduate
 */
public class DealController {
    final   private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public  String  getDealsToJSON( )
    {
        String result = null;
 
        ArrayList<Deal> theDeals = ash.getDeals();
                
        if( theDeals != null)
            result = DealHandler.getLoader().toJSON(theDeals);

        return result;
    }
}
