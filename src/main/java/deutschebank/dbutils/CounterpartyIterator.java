/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Sofia
 */
public class CounterpartyIterator
{
   ResultSet rowIterator;

   CounterpartyIterator( ResultSet rs )
   {
       rowIterator = rs;
   }
   
   public boolean  first() throws SQLException
   {
      return rowIterator.first();
   }
   
   public boolean last() throws SQLException
   {
      return rowIterator.last();
   }
   public boolean next() throws SQLException
   {
      return rowIterator.next();
   }
   
   public boolean prior() throws SQLException
   {
      return rowIterator.previous();
   }

   public   String  getCounterpartyName() throws SQLException
   {
      return rowIterator.getString("counterparty_name");
   }

   public   int  getCounterpartyID() throws SQLException
   {
      return rowIterator.getInt("counterparty_id");
   }
   
   public   String  getCounterpartyStatus() throws SQLException
   {
      return rowIterator.getString("counterparty_status");
   }

   public   String  getCounterpartyDateRegistered() throws SQLException
   {
      return rowIterator.getString("counterparty_date_registered");
   }

   Counterparty   buildCounterparty() throws SQLException
   {
       Counterparty result = new Counterparty( getCounterpartyID(), getCounterpartyName(), getCounterpartyStatus(), getCounterpartyDateRegistered() );
       
       return result;
   }
}
