package deutschebank.dbutils;

import java.util.ArrayList;

import junit.framework.TestCase;

public class CounterpartyTest extends TestCase {
	
	 private static final int     COUNTERPARTY_ID = 0;   
	 private static final String  COUNTERPARTY_NAME = "Lina";
	 private static final String  COUNTERPARTY_STATUS = "A";
	 private static final String  COUNTERPARTY_DATE_REGISTERED = "01/01/2014  00:00:00";
	 
	 private Counterparty counterparty;

    @Override
    public void setUp()
    {
    	counterparty = new Counterparty(COUNTERPARTY_ID, COUNTERPARTY_NAME, 
    			COUNTERPARTY_STATUS, COUNTERPARTY_DATE_REGISTERED);
    	
    	assertNotNull(counterparty);
    }
    
    public void testGetCounterpartyID()
    {
    	assertEquals(COUNTERPARTY_ID, counterparty.getCounterpartyID());
    }
    
    public void testGetCounterpartyName()
    {
    	assertEquals(COUNTERPARTY_NAME, counterparty.getCounterpartyName());
    }
    
    public void testGetCounterpartyStatus()
    {
    	assertEquals(COUNTERPARTY_STATUS, counterparty.getCounterpartyStatus());
    }
    
    public void testGetCounterpartyDateRegistered()
    {
    	assertEquals(COUNTERPARTY_DATE_REGISTERED, counterparty.getCounterpartyDateRegistered());
    }
    
    public void testSetCounterpartyID()
    {
    	int newID = 1;
    	counterparty.setCounterpartyID(newID);
    	assertEquals(newID, counterparty.getCounterpartyID());
    }
    
    public void testSetCounterpartyName()
    {
    	String newCounterpartyName = "Richard";
    	counterparty.setCounterpartyName(newCounterpartyName);
    	assertEquals(newCounterpartyName, counterparty.getCounterpartyName());
    }
   
    public void testSetCounterpartyStatus()
    {
    	String newCounterpartyStatus = "B";
    	counterparty.setCounterpartyStatus(newCounterpartyStatus);
    	assertEquals(newCounterpartyStatus, counterparty.getCounterpartyStatus());
    }
    
    public void testSetCounterpartyDateRegistered()
    {
    	String newCounterpartyDateRegistered = "01/09/2018  00:00:00";
    	counterparty.setCounterpartyDateRegistered(newCounterpartyDateRegistered);
    	assertEquals(newCounterpartyDateRegistered, counterparty.getCounterpartyDateRegistered());
    }
    
    public void testCounterpartyToJson() {
        String result = CounterpartyHandler.getLoader().toJSON(counterparty);
        assertNotNull(result);
    }


    public void testArrayListCounterpartyToJson() {
        ArrayList<Counterparty> testCounterparties = new ArrayList<Counterparty>();
        testCounterparties.add(counterparty);
        testCounterparties.add(new Counterparty(1, "Lewis",
    			"B", "01/01/2003  00:00:00"));
        String result = CounterpartyHandler.getLoader().toJSON(testCounterparties);
        assertNotNull(result);
    }
    
}
