package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.jmock.Mockery;
import junit.framework.TestCase;
import org.jmock.Expectations;
import static org.junit.Assert.assertNotEquals;

public class CounterpartyIteratorTest extends TestCase {
	
    private static final String TEST_COUNTERPARTY_NAME = "MockName";
    private static final int TEST_COUNTERPARTY_ID = 42;
    private static final String TEST_COUNTERPARTY_STATUS = "MockStatus";
    private static final String TEST_DATE_REGISTERED = "01/01/2014 00:00:00";

    private static final boolean TEST_BOOLEAN_VALUE = true;
	
	Mockery context = new Mockery();
	final ResultSet rowIterator = context.mock(ResultSet.class);
	
	public void testCounterpartyIteratorGetCounterpartyName() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("counterparty_name"); will(returnValue(TEST_COUNTERPARTY_NAME));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_COUNTERPARTY_NAME, testCounterpartyIterator.getCounterpartyName());
	}

	public void testCounterpartyIteratorGetCounterpartyID() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getInt("counterparty_id"); will(returnValue(TEST_COUNTERPARTY_ID));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_COUNTERPARTY_ID, testCounterpartyIterator.getCounterpartyID());
	}
	
	public void testCounterpartyIteratorGetCounterpartyStatus() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("counterparty_status"); will(returnValue(TEST_COUNTERPARTY_STATUS));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_COUNTERPARTY_STATUS, testCounterpartyIterator.getCounterpartyStatus());
	}
	
	public void testCounterpartyIteratorGetDateRegistered() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("counterparty_date_registered"); will(returnValue(TEST_DATE_REGISTERED));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_DATE_REGISTERED, testCounterpartyIterator.getCounterpartyDateRegistered());
	}
	
	public void testCounterpartyIteratorFirst() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).first(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCounterpartyIterator.first());
	} 
	
	public void testCounterpartyIteratorLast() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).last(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCounterpartyIterator.last());
	} 
	
	public void testCounterpartyIteratorNext() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).next(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCounterpartyIterator.next());
	} 
	
	public void testCounterpartyIteratorPrior() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).previous(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CounterpartyIterator testCounterpartyIterator = new CounterpartyIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCounterpartyIterator.prior());
	} 
	
} 
