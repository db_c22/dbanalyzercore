package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.jmock.Mockery;
import junit.framework.TestCase;
import org.jmock.Expectations;
import static org.junit.Assert.assertNotEquals;

public class CatalogInfoIteratorTest extends TestCase {

    private static final String TEST_TABLE_SCHEM = "MockSchem";
    private static final String TEST_TABLE_CATALOG = "MockTableCatalog";

    private static final boolean TEST_BOOLEAN_VALUE = true;
	
	Mockery context = new Mockery();
	final ResultSet rowIterator = context.mock(ResultSet.class);
	
	public void testCatalogInfoIteratorGetTableSchema() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("TABLE_SCHEM"); will(returnValue(TEST_TABLE_SCHEM));
		}});
        
        context.assertIsSatisfied();
        CatalogInfoIterator testCatalogInfoIterator = new CatalogInfoIterator(rowIterator);
        assertEquals(TEST_TABLE_SCHEM, testCatalogInfoIterator.getTable_Schema());
	}
	
	public void testCatalogInfoIteratorGetTableCatalog() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("TABLE_CATALOG"); will(returnValue(TEST_TABLE_CATALOG));
		}});
        
        context.assertIsSatisfied();
        CatalogInfoIterator testCatalogInfoIterator = new CatalogInfoIterator(rowIterator);
        assertEquals(TEST_TABLE_CATALOG, testCatalogInfoIterator.getTable_Catalog());
	}
	
	public void testCatalogInfoIteratorFirst() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).first(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CatalogInfoIterator testCatalogInfoIterator = new CatalogInfoIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCatalogInfoIterator.first());
	} 
	
	public void testCatalogInfoIteratorLast() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).last(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CatalogInfoIterator testCatalogInfoIterator = new CatalogInfoIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCatalogInfoIterator.last());
	} 
	
	public void testCatalogInfoIteratorNext() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).next(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CatalogInfoIterator testCatalogInfoIterator = new CatalogInfoIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCatalogInfoIterator.next());
	} 
	
	public void testCatalogInfoIteratorPrior() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).previous(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        CatalogInfoIterator testCatalogInfoIterator = new CatalogInfoIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testCatalogInfoIterator.prior());
	} 
}
