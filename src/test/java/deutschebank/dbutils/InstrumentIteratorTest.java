package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jmock.Mockery;
import org.jmock.Expectations;
import static org.junit.Assert.assertNotEquals;

import junit.framework.TestCase;

public class InstrumentIteratorTest extends TestCase {
	
    private static final int TEST_INSTRUMENT_ID = 0;
    private static final String TEST_INSTRUMENT_NAME = "MockName";
    private static final boolean TEST_BOOLEAN_VALUE = true;
    
	Mockery context = new Mockery();
	final ResultSet rowIterator = context.mock(ResultSet.class);
	InstrumentIterator testInstrumentIterator = new InstrumentIterator(rowIterator);

	
	public void testDealIteratorGetInstrumentName() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("instrument_name"); will(returnValue(TEST_INSTRUMENT_NAME));
		}});
        
        context.assertIsSatisfied();
        assertEquals(TEST_INSTRUMENT_NAME, testInstrumentIterator.getInstrumentName());
	}
	
	public void testDealIteratorGetInstrumentID() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getInt("instrument_id"); will(returnValue(TEST_INSTRUMENT_ID));
		}});
        
        context.assertIsSatisfied();
        assertEquals(TEST_INSTRUMENT_ID, testInstrumentIterator.getInstrumentID());
	}
	
	public void testInstrumentIteratorFirst() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).first(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        InstrumentIterator testInstrumentIterator = new InstrumentIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testInstrumentIterator.first());
	} 
	
	public void testInstrumentIteratorLast() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).last(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        InstrumentIterator testInstrumentIterator = new InstrumentIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testInstrumentIterator.last());
	} 
	
	public void testInstrumentIteratorNext() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).next(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        InstrumentIterator testInstrumentIterator = new InstrumentIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testInstrumentIterator.next());
	} 
	
	public void testInstrumentIteratorPrior() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).previous(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        InstrumentIterator testInstrumentIterator = new InstrumentIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testInstrumentIterator.prior());
	} 
}
