/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.core;

import deutschebank.MainUnit;
import deutschebank.dbutils.DBConnector;
import deutschebank.dbutils.Instrument;
import deutschebank.dbutils.Counterparty;
import deutschebank.dbutils.Deal;
import deutschebank.dbutils.User;
import deutschebank.dbutils.UserHandler;
import deutschebank.dbutils.InstrumentHandler;
import deutschebank.dbutils.CounterpartyHandler;
import deutschebank.dbutils.DealHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class ApplicationScopeHelper
{

    private String itsInfo = "NOT SET";
    private DBConnector itsConnector = null;
    private static final String DBName = "db_grad_cs_1917";
    

    public String getInfo()
    {
        return itsInfo;
    }
    
    public DBConnector getDBConnector()
    {
        return itsConnector;
    }

    public void setInfo(String itsInfo)
    {
        this.itsInfo = itsInfo;
    }
    
    public boolean bootstrapDBConnection()
    {
        boolean result = false;
        if (itsConnector == null)
        {
            try
            {
                itsConnector = DBConnector.getConnector();

                PropertyLoader pLoader = PropertyLoader.getLoader();

                Properties pp;
                pp = pLoader.getPropValues("dbConnector.properties");

                result = itsConnector.connect(pp);
            } catch (IOException ex)
            {
                Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
            result = true;
        
        return result;
    }
    
    public User userLogin( String userId, String userPwd )
    {
        User theUser = null;
        try
        {
            UserHandler theUserHandler = UserHandler.getLoader();
            
            theUser = theUserHandler.loadFromDB(DBConnector.getConnector().getConnection(), userId, userPwd );
            
            if( theUser != null )
                MainUnit.log( "User " + userId + " has logged into system");
        } catch (IOException ex)
        {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return theUser;
    }
    
    
     public ArrayList<Instrument> getInstruments()
    {
        ArrayList<Instrument> instrumentsArrayList = null;
        try
        {
            InstrumentHandler theInstrumentHandler = InstrumentHandler.getLoader();
            
            instrumentsArrayList = theInstrumentHandler.loadFromDB(DBName, DBConnector.getConnector().getConnection());
            
            if( instrumentsArrayList != null )
                MainUnit.log( "Data for Instruments is LOADED!");
        } catch (IOException ex)
        {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return instrumentsArrayList;
    }
    
     
      public ArrayList<Counterparty> getCounterparties()
    {
        ArrayList<Counterparty> counterpartiesArrayList = null;
        try
        {
            CounterpartyHandler theCounterpartiesHandler = CounterpartyHandler.getLoader();
            
            counterpartiesArrayList = theCounterpartiesHandler.loadFromDB(DBName, DBConnector.getConnector().getConnection());
            
            if( counterpartiesArrayList != null )
                MainUnit.log( "Data for Counterparties is LOADED!");
        } catch (IOException ex)
        {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return counterpartiesArrayList;
    }
      
       public ArrayList<Deal> getDeals()
    {
        ArrayList<Deal> dealsArrayList = null;
        try
        {
            DealHandler theDealHandler = DealHandler.getLoader();
            
            dealsArrayList = theDealHandler.loadFromDB(DBName, DBConnector.getConnector().getConnection());
            
            if( dealsArrayList != null )
                MainUnit.log( "Data for Deals is LOADED!");
        } catch (IOException ex)
        {
            Logger.getLogger(ApplicationScopeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dealsArrayList;
    }

}
