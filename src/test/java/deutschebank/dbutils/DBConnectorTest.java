package deutschebank.dbutils;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import junit.framework.TestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class DBConnectorTest extends TestCase {
	
	public void testDBConnectorEmpty() throws IOException {
		assertNotNull(DBConnector.getConnector().getConnector());
		assertNull(DBConnector.getConnector().getConnection());
		PropertyLoader pLoader = PropertyLoader.getLoader();
		assertNotNull(pLoader);
		Properties pp = pLoader.getPropValues( "dbConnector.properties" );
	}

}