/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

/**
 *
 * @author Sofia
 */
public class Deal
{
    private int     itsDealID;
    private String  itsDealTime;
    private int  	itsDealCounterpartyID;
    private int  	itsDealInstrumentID;
    private String  itsDealType;
    private int  	itsDealAmount;
    private int  	itsDealQuantity;
    
    public  Deal( int id, String time, int counterparty_id, int instrument_id, String type, int amount, int quantity)
    {
        itsDealID = id;
        itsDealTime = time;
        itsDealCounterpartyID = counterparty_id;
        itsDealInstrumentID = instrument_id;
        itsDealType = type;
        itsDealAmount = amount;
        itsDealQuantity = quantity;
    }
    
    public int getDealID()
    {
        return itsDealID;
    }

    public void setDealID(int itsDealID)
    {
        this.itsDealID = itsDealID;
    }

    public String getDealTime()
    {
        return itsDealTime;
    }

    public void setDealTime(String itsDealTime)
    {
        this.itsDealTime = itsDealTime;
    }   
    
    public int getDealCounterpartyID()
    {
        return itsDealCounterpartyID;
    }

    public void setDealCounterpartyID(int itsDealCounterpartyID)
    {
        this.itsDealCounterpartyID = itsDealCounterpartyID;
    }
    
    public int getDealInstrumentID()
    {
        return itsDealInstrumentID;
    }

    public void setDealInstrumentID(int itsDealInstrumentID)
    {
        this.itsDealInstrumentID = itsDealInstrumentID;
    }
    
    public String getDealType()
    {
        return itsDealType;
    }

    public void setDealType(String itsDealType)
    {
        this.itsDealType = itsDealType;
    }
    
    public int getDealAmount()
    {
        return itsDealAmount;
    }

    public void setDealAmount(int itsDealAmount)
    {
        this.itsDealAmount = itsDealAmount;
    }
    
    public int getDealQuantity()
    {
        return itsDealQuantity;
    }

    public void setDealQuantity(int itsDealQuantity)
    {
        this.itsDealQuantity = itsDealQuantity;
    }
}
