package deutschebank.dbutils;

import static org.junit.Assert.assertNotEquals;

import junit.framework.TestCase;

public class UserTest extends TestCase {

    private static final String TEST_USER_ID = "MockID";
    private static final String TEST_USER_PWD = "MockPassword";
    private User user;

    @Override
    protected void setUp() throws Exception {
        user = new User(TEST_USER_ID, TEST_USER_PWD);
        assertNotNull(user);
    }


    public void testUserGetId() {
        assertEquals(TEST_USER_ID, user.getUserID());
    }

    public void testUserGetUserPwd() {
        assertEquals(TEST_USER_PWD, user.getUserPwd());
    }


    public void testUserSetId() {
        String newId = "NewMockId";
        user.setUserID(newId);
        assertNotEquals(TEST_USER_ID, user.getUserID());
        assertEquals(newId, user.getUserID());
    }

    public void testUserSetUserName() {
        String newPassword = "NewMockPwd";
        user.setUserPwd(newPassword);
        assertNotEquals(TEST_USER_PWD, user.getUserPwd());
        assertEquals(newPassword, user.getUserPwd());
    }

    public void testUserToJson() {
        String result = UserHandler.getLoader().toJSON(user);
        assertNotNull(result);

    }
}
