/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

/**
 *
 * @author Sofia
 */
public class Counterparty
{
    private int     itsCounterpartyID;
    private String  itsCounterpartyName;
    private String  itsCounterpartyStatus;
    private String  itsCounterpartyDateRegistered;
    
    public  Counterparty( int id, String name, String status, String date_registered)
    {
        itsCounterpartyID = id;
        itsCounterpartyName = name;
        itsCounterpartyStatus = status;
        itsCounterpartyDateRegistered = date_registered;
    }
    
    public int getCounterpartyID()
    {
        return itsCounterpartyID;
    }

    public void setCounterpartyID(int itsCounterpartyID)
    {
        this.itsCounterpartyID = itsCounterpartyID;
    }

    public String getCounterpartyName()
    {
        return itsCounterpartyName;
    }

    public void setCounterpartyName(String itsCounterpartyName)
    {
        this.itsCounterpartyName = itsCounterpartyName;
    }
    
    public String getCounterpartyStatus()
    {
        return itsCounterpartyStatus;
    }

    public void setCounterpartyStatus(String itsCounterpartyStatus)
    {
        this.itsCounterpartyStatus = itsCounterpartyStatus;
    }
    
    public String getCounterpartyDateRegistered()
    {
        return itsCounterpartyDateRegistered;
    }

    public void setCounterpartyDateRegistered(String itsCounterpartyDateRegistered)
    {
        this.itsCounterpartyDateRegistered = itsCounterpartyDateRegistered;
    }
}
