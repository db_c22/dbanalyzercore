package deutschebank.dbutils;

import org.junit.Test;

import java.util.ArrayList;

import junit.framework.TestCase;

import static org.junit.Assert.*;

import java.util.ArrayList;

public class InstrumentTest extends TestCase {

    private static final int TEST_INSTRUMENT_ID = 0;
    private static final String TEST_INSTRUMENT_NAME = "MockName";
    protected Instrument instrument;

    @Override
    protected void setUp() throws Exception {
        instrument = new Instrument(TEST_INSTRUMENT_ID, TEST_INSTRUMENT_NAME);
        assertNotNull(instrument);
    }

    public void testInstrumentGetId() {
        assertEquals(TEST_INSTRUMENT_ID, instrument.getInstrumentID());
    }

    public void testInstrumentGetInstrumentName() {
        assertEquals(TEST_INSTRUMENT_NAME, instrument.getInstrumentName());
    }

    public void testInstrumentSetId() {
        int newId = 1;
        instrument.setInstrumentID(newId);
        assertNotEquals(TEST_INSTRUMENT_ID, instrument.getInstrumentID());
        assertEquals(newId, instrument.getInstrumentID());
    }

    public void testInstrumentSetInstrumentName() {
        String newName = "NewMockName";
        instrument.setInstrumentName(newName);
        assertNotEquals(TEST_INSTRUMENT_NAME, instrument.getInstrumentName());
        assertEquals(newName, instrument.getInstrumentName());
    }

    public void testInstrumentToJson() {
        String result = InstrumentHandler.getLoader().toJSON(instrument);
        assertNotNull(result);
    }


    public void testArrayListInstrumentsToJson() {
        ArrayList<Instrument> testInstruments = new ArrayList<Instrument>();
        testInstruments.add(instrument);
        testInstruments.add(new Instrument(1, "AnotherMock"));
        String result = InstrumentHandler.getLoader().toJSON(testInstruments);
        assertNotNull(result);
    }
}
