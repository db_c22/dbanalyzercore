/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.dbutils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import deutschebank.MainUnit;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sofia
 */
public class CounterpartyHandler
{
    static  private CounterpartyHandler itsSelf = null;
    
    public CounterpartyHandler(){}
    
    static  public  CounterpartyHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new CounterpartyHandler();
        return itsSelf;
    }
    
    public  Counterparty  loadFromDB( String dbID, Connection theConnection, int key )
    {
        Counterparty result = null;
        try
        {
            String sbQuery = "select * from " + dbID + ".counterparty where counterparty_id=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, key);
            ResultSet rs = stmt.executeQuery();
            
            CounterpartyIterator iter = new CounterpartyIterator(rs);
            
            while( iter.next() )
            {
                result = iter.buildCounterparty();
                if(MainUnit.debugFlag)
                    System.out.println( result.getCounterpartyID() + "//" + result.getCounterpartyName() + "//" + result.getCounterpartyStatus() + "//" + result.getCounterpartyDateRegistered());
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }

    public  ArrayList<Counterparty>  loadFromDB( String dbID, Connection theConnection, int lowerKey, int upperKey )
    {
        ArrayList<Counterparty> result = new ArrayList<Counterparty>();
        Counterparty theCounterparty = null;
        try
        {
            String sbQuery = "select * from " + dbID + ".counterparty where counterparty_id>=? and counterparty_id<=?";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            stmt.setInt(1, lowerKey);
            stmt.setInt(2, upperKey);
            ResultSet rs = stmt.executeQuery();
            
            CounterpartyIterator iter = new CounterpartyIterator(rs);
            
            while( iter.next() )
            {
                theCounterparty = iter.buildCounterparty();
                if(MainUnit.debugFlag)
                    System.out.println( theCounterparty.getCounterpartyID() + "//" + theCounterparty.getCounterpartyName() + "//" + theCounterparty.getCounterpartyStatus() + "//" + theCounterparty.getCounterpartyDateRegistered());
                result.add(theCounterparty);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  ArrayList<Counterparty>  loadFromDB( String dbID, Connection theConnection )
    {
        ArrayList<Counterparty> result = new ArrayList<Counterparty>();
        Counterparty theCounterparty = null;
        try
        {
            String sbQuery = "select * from " + dbID + ".counterparty";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);            
            ResultSet rs = stmt.executeQuery();
            
            CounterpartyIterator iter = new CounterpartyIterator(rs);
            
            while( iter.next() )
            {
                theCounterparty = iter.buildCounterparty();
                if(MainUnit.debugFlag)
                    System.out.println( theCounterparty.getCounterpartyID() + "//" + theCounterparty.getCounterpartyName()  + "//" + theCounterparty.getCounterpartyStatus() + "//" + theCounterparty.getCounterpartyDateRegistered());
                result.add(theCounterparty);
            }
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return result;
    }
    
    public  String  toJSON( Counterparty theCounterparty )
    {
        String result = "";
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(theCounterparty);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public  String  toJSON( ArrayList<Counterparty> theCounterparties )
    {
        String result = "";
        Counterparty[] insArray = new Counterparty[theCounterparties.size()];
        theCounterparties.toArray(insArray);
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(insArray);
        } 
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
