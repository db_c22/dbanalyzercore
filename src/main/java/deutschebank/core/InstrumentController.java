/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.core;

import deutschebank.dbutils.Instrument;
import deutschebank.dbutils.InstrumentHandler;
import deutschebank.dbutils.User;
import deutschebank.dbutils.UserHandler;
import java.util.ArrayList;

/**
 *
 * @author Graduate
 */
public class InstrumentController {
    final   private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public  String  getInstrumentsToJSON( )
    {
        String result = null;
 
        ArrayList<Instrument> theInstruments = ash.getInstruments();
                
        if( theInstruments != null)
            result = InstrumentHandler.getLoader().toJSON(theInstruments);

        return result;
    }
}
