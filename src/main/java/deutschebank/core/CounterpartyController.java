/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deutschebank.core;

import deutschebank.dbutils.Counterparty;
import deutschebank.dbutils.CounterpartyHandler;
import java.util.ArrayList;

/**
 *
 * @author Graduate
 */
public class CounterpartyController {

    final   private ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public  String  getCounterpartiesToJSON( )
    {
        String result = null;
 
        ArrayList<Counterparty> theCounterparties = ash.getCounterparties();
                
        if( theCounterparties != null)
            result = CounterpartyHandler.getLoader().toJSON(theCounterparties);

        return result;
    }
}
