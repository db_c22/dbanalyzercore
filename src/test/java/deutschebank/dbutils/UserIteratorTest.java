package deutschebank.dbutils;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jmock.Mockery;
import org.jmock.Expectations;
import static org.junit.Assert.assertNotEquals;

import junit.framework.TestCase;

public class UserIteratorTest extends TestCase {
	
    private static final String TEST_USER_ID = "MockID";
    private static final String TEST_USER_PWD = "MockPassword";
    private static final boolean TEST_BOOLEAN_VALUE = true;
	
	Mockery context = new Mockery();
	final ResultSet rowIterator = context.mock(ResultSet.class);
	
	public void testUserIteratorGetUserId() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("user_id"); will(returnValue(TEST_USER_ID));
		}});
        
        context.assertIsSatisfied();
        UserIterator testUserIterator = new UserIterator(rowIterator);
        assertEquals(TEST_USER_ID, testUserIterator.getUserId());
	}
	
	public void testUserIteratorGetUserPwd() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).getString("user_pwd"); will(returnValue(TEST_USER_PWD));
		}});
        
        context.assertIsSatisfied();
        UserIterator testUserIterator = new UserIterator(rowIterator);
        assertEquals(TEST_USER_PWD, testUserIterator.getUserPwd());
	}
	
	public void testUserIteratorFirst() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).first(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        UserIterator testUserIterator = new UserIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testUserIterator.first());
	} 
	
	public void testUserIteratorLast() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).last(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        UserIterator testUserIterator = new UserIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testUserIterator.last());
	} 
	
	public void testUserIteratorNext() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).next(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        UserIterator testUserIterator = new UserIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testUserIterator.next());
	} 
	
	public void testUserIteratorPrior() throws SQLException {		
		context.checking(new Expectations() {{
		    allowing(rowIterator).previous(); will(returnValue(TEST_BOOLEAN_VALUE));
		}});
        
        context.assertIsSatisfied();
        UserIterator testUserIterator = new UserIterator(rowIterator);
        assertEquals(TEST_BOOLEAN_VALUE, testUserIterator.prior());
	} 

}
